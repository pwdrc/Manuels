# Minhas coisas de D&D para compartilhar

## Planilha do GoogleDocs que Joga os dados e resolve o combate automaticamente: 
- Link - https://docs.google.com/spreadsheets/d/1LCnvWlR4GlQn-EBaLcu-vStrS1Jx0V1hpHV5u9_aULQ/edit?usp=sharing

## Minhas Fichas de Personagens com tudo calculado automaticamente: 
- Essa ficha foi adaptada e melhorada a partir do trabalho de [rivera.ga@gmail.com] em (sheet: https://docs.google.com/spreadsheets/d/1liFz7qr52p0UED4_JWh9UJsavuJ4nViZCMrTv_SXRRo/edit)

### Link minha adaptação (High Elf Wizard) 
- https://docs.google.com/spreadsheets/d/1jTjUXH-qRSGBHiJXUCFd9U2vAoOmLIcT6F5tDD11EQA/edit?usp=sharing

### Link minha adaptação (Human Fighter) 
- https://docs.google.com/spreadsheets/d/1AcpeaFc1JJzyHSAEBTe-VH2UnrgOYpzskrF5xevghpI/edit?usp=sharing

### Link minha adaptação (Wood Elf Druid) 
- https://docs.google.com/spreadsheets/d/1ILGFPKNkXPwFhJnWmitS8szvpooPDnYuAnZeLVUqkQM/edit?usp=sharing

### Link minha adaptação (Tabaxi Roge) 
- https://docs.google.com/spreadsheets/d/1nqyfLoT3Fh9OJWTYj2HZHZDcGRycAElXrMWrHFb1eNU/edit?usp=sharing

## Dicas Gerais

### Dicas de App Scrip:
- Tutorial: https://www.youtube.com/watch?v=f2dQQKK-xLc
- Link para a planilha: https://docs.google.com/spreadsheets/d/10ZjEZmY1yRsB8rTsC39W4Ky2pDzF0zH67MksP7ywzX0/edit#gid=2120758880


